# Where Are My Pants

A project for finding things

## Current State

Nothing is implemented yet

## Planned features

- `whereis` command: Find a named resource
- `whereami` command: provide clean info about current machine, directory, and git status/remotes (prototype available as an alias command)
- `thereis` command: Provide a location for a named resource
- `hereis` command: An alias for `thereis` that provides $(pwd) as the second argument

- git integrations -- keep track of local git repos and:
  - Whether they have been synced to a remote
  - whether there are duplicate repos (in different or identical states)

- profiles: a way to track context-specific dotfiles and installations

